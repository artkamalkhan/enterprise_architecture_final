import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import math


from sklearn import preprocessing
from sklearn.metrics import accuracy_score,confusion_matrix,mean_squared_error
from sklearn.model_selection import KFold,cross_val_score
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import AdaBoostClassifier,RandomForestClassifier


le = preprocessing.LabelEncoder()
le.fit(['B','M'])
file_name = 'dataset.txt'
X = []
Y = []
class_names = ['Benign','Malignant']
class_names = np.array(class_names)

names=["ID", "Diagnosis", "F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","F16","F17","F18","F19","F20","F21","F22","F23","F24","F25","F26","F27","F28","F29","F30"]
name = ['Diagnosis'] #Target column name

df = pd.read_csv(file_name, sep=",", header=None, names=names)

X = pd.DataFrame(df).drop('Diagnosis',axis=1).drop('ID',axis=1).drop('F5',axis=1).drop('F6',axis=1).drop('F9',axis=1).drop('F10',axis=1).drop('F12',axis=1).drop('F15',axis=1).drop('F16',axis=1).drop('F17', axis=1).drop('F18',axis=1).drop('F19',axis=1).drop('F20',axis=1).drop('F22',axis=1).drop('F25',axis=1).drop('F26',axis=1).drop('F27',axis=1).drop('F28',axis=1).drop('F29',axis=1).drop('F30',axis=1)
Y = df['Diagnosis']

# read = X.as_matrix()[3]
# for i in read:
#     print i

def log_reg(X_digit,Y_digit):
    log_penalty = ['l1','l2']
    log_dual = [False]
    log_C = [0.001,0.01,0.1]
    log_class_weight = ['balanced']
    log_max_iter = [100,200]
    log_random_state = [5,10,15]
    log_solver = ['newton-cg','lbfgs','liblinear','sag']
    log_multi_class = ['ovr','multinomial']
    log_warm_start = [True, False]
    log_score = {}
    for i in log_penalty:
        for j in log_dual:
            for k in log_C:
                for l in log_class_weight:
                    for o in log_max_iter:
                        for p in log_random_state:
                            for u in log_solver:
                                for y in log_multi_class:
                                    for m in log_warm_start:
                                        for kFoldValue in range(2,13):
                                            temp = {}
                                            if(u=='liblinear' and y=='multinomial'):
                                                LR = LogisticRegression(penalty=i, dual=j, C=k, class_weight=l,
                                                                        max_iter=o, random_state=p, solver=u, warm_start=m)
                                            elif(u!='liblinear' and i=='l2'):
                                                LR = LogisticRegression(penalty=i, dual=j, C=k, class_weight=l,
                                                                        max_iter=o, random_state=p, solver=u,
                                                                        multi_class=y, warm_start=m)
                                            else:
                                                LR = LogisticRegression( dual=j, C=k, class_weight=l,
                                                                        max_iter=o, random_state=p, solver=u,
                                                                        multi_class=y, warm_start=m)
                                            temp['penalty'] = i
                                            temp['dual'] = j
                                            temp['C'] = k
                                            temp['class_weight'] = l
                                            temp['max_iter'] = o
                                            temp['random_state'] = p
                                            temp['solver'] = u
                                            temp['multi_class'] = y
                                            temp['warm_start'] = m
                                            kf = KFold(n_splits=kFoldValue)
                                            acc_score_temp = []
                                            for train_indices, test_indices in kf.split(X_digit):
                                                x_train, y_train, x_test, y_test = [], [], [], []
                                                for arr in X_digit[train_indices]:
                                                    x_train.append(arr[0:-1])
                                                for arr in Y_digit[train_indices]:
                                                    y_train.append(arr[0])
                                                for arr in X_digit[test_indices]:
                                                    x_test.append(arr[0:-1])
                                                for arr in Y_digit[test_indices]:
                                                    y_test.append(arr[0])
                                                x_train = np.array(x_train)
                                                y_train = np.array(y_train)
                                                x_test = np.array(x_test)
                                                y_test = np.array(y_test)

                                                LR.fit(x_train, y_train)
                                                predicted = LR.predict(x_test)
                                                acc = accuracy_score(y_test, predicted)
                                                acc_score_temp.append(acc)
                                            temp['kFold_split']=kFoldValue
                                            if(max(acc_score_temp)!=1.0):
                                                log_score[str(temp)] = max(acc_score_temp)
                                                print temp
                                                print max(acc_score_temp)
                                            else:
                                                pass
    return log_score

# b = log_reg(X.as_matrix(),Y.as_matrix())
# a = sorted(b.iteritems(), key=lambda key_value: key_value[0])
# print a[0]
# ("{'warm_start': False, 'kFold_split': 10, 'C': 0.001, 'solver': 'lbfgs', 'max_iter': 100, 'penalty': 'l1', 'multi_class': 'multinomial', 'random_state': 10, 'dual': False, 'class_weight': 'balanced'}", 0.98245614035087714)




def score_best_acc(X_digit,Y_digit,model):
    a = datetime.datetime.now()
    best_acc = {'LOG':0.964912280702,'MLP':0.982456140351,'KNN':0.964912280702,'DT':0.964912280702,'NB':0.982142857143,'SVC':0.964912280702,'ADA':0.982456140351 ,'RAND':0.982456140351}

    info = {}
    kFoldValue = 10
    kf = KFold(n_splits=kFoldValue)

    if (model=='LOG'):
        MODEL = LogisticRegression(warm_start=False,C=0.001,solver='lbfgs',max_iter=100,penalty='l2',multi_class='multinomial',random_state=10,dual=False,class_weight='balanced')
    elif(model=='MLP'):
        MODEL = MLPClassifier(hidden_layer_sizes=100, learning_rate='adaptive', max_iter=200, solver='sgd',activation='identity', momentum=0.3)
    elif(model=='KNN'):
        MODEL = KNeighborsClassifier(n_neighbors=3, weights='distance', leaf_size=30 , p=30)
    elif(model=='DT'):
        MODEL = DecisionTreeClassifier(min_samples_split=10, splitter='best', criterion= 'gini', presort=False)
    elif(model=='NB'):
        MODEL = GaussianNB()
    elif(model=='SVC'):
        MODEL = SVC(kernel='linear',C=0.01,degree=1,probability=True,verbose=False,shrinking=False,class_weight='balanced')
    elif(model=='ADA'):
        MODEL = AdaBoostClassifier(n_estimators=100, algorithm='SAMME',
                                   base_estimator=DecisionTreeClassifier(min_samples_split=10, splitter='best',
                                                                         criterion='gini', presort=False))
    elif(model=='RAND'):
        MODEL = RandomForestClassifier(warm_start=False, oob_score=False, max_features='auto',bootstrap=True, n_estimators=10, criterion='entropy', verbose=0)
    else:
        pass
    for train_indices, test_indices in kf.split(X_digit):
        x_train, y_train, x_test, y_test = [], [], [], []
        for arr in X_digit[train_indices]:
            x_train.append(arr[0:])
        for arr in Y_digit[train_indices]:
            y_train.append(arr[0])
        for arr in X_digit[test_indices]:
            x_test.append(arr[0:])
        for arr in Y_digit[test_indices]:
            y_test.append(arr[0])
        x_train = np.array(x_train)
        y_train = np.array(y_train)
        x_test = np.array(x_test)
        y_test = np.array(y_test)

        MODEL.fit(x_train, y_train)
        predicted = MODEL.predict(x_test)
        acc = accuracy_score(y_test, predicted)
        # print acc
        if((float(format(acc,'.12f'))==best_acc[model]) and acc != 1.0):
            b = datetime.datetime.now()
            delta = b - a
            timeForExec = int(delta.total_seconds() * 1000)
            print acc
            tn, fp, fn, tp = confusion_matrix(y_test, predicted).ravel()
            temp_test = le.transform(y_test)
            temp_predicted = le.transform(predicted)
            print 'BreakPoint'
            rms = math.sqrt(mean_squared_error(temp_test, temp_predicted))
            specificity = tn * 1.0 /(tn+fp)
            precision = tp * 1.0/(tp+fp)
            recall = tp * 1.0/(tp+fn)
            f1_score = 2*tp*1.0/(2*tp + fp + fn)
            # print rms, specificity, precision, recall, f1_score
            info['RMSE'] = rms
            info['Specificity'] = specificity
            info['Precision'] = precision
            info['Recall'] = recall
            info['F1 score'] = f1_score
            info['Build time'] = timeForExec
            train_acc_list = {}
            test_acc_list = {}
            for innerKfoldValue in range(2,13):
                temp_train_dict = {}
                temp_test_dict = {}
                inner_kf = KFold(n_splits=innerKfoldValue)
                train_acc_arr = []
                test_acc_arr = []
                for inner_train_indices, inner_test_indices in inner_kf.split(X_digit):
                    inner_x_train, inner_y_train, inner_x_test, inner_y_test = [], [], [], []
                    for arr in X_digit[inner_train_indices]:
                        inner_x_train.append(arr[0:])
                    for arr in Y_digit[inner_train_indices]:
                        inner_y_train.append(arr[0])
                    for arr in X_digit[inner_test_indices]:
                        inner_x_test.append(arr[0:])
                    for arr in Y_digit[inner_test_indices]:
                        inner_y_test.append(arr[0])
                    inner_x_train = np.array(inner_x_train)
                    inner_y_train = np.array(inner_y_train)
                    inner_x_test = np.array(inner_x_test)
                    inner_y_test = np.array(inner_y_test)

                    train_predicted = MODEL.predict(inner_x_train)
                    test_predicted = MODEL.predict(inner_x_test)

                    train_acc = accuracy_score(inner_y_train,train_predicted)
                    test_acc = accuracy_score(inner_y_test,test_predicted)

                    train_acc_arr.append(train_acc)
                    test_acc_arr.append(test_acc)

                temp_train_dict['acc_scores'] = train_acc_arr
                temp_train_dict['avg_acc_score'] = min(train_acc_arr)

                temp_test_dict['acc_scores'] = test_acc_arr
                temp_test_dict['avg_acc_score'] = min(test_acc_arr)

                train_acc_list[innerKfoldValue] = temp_train_dict
                test_acc_list[innerKfoldValue] = temp_test_dict

            return train_acc_list,test_acc_list,info


main_info = {}
log_train_acc,log_test_acc,main_info['LOG'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'LOG')
log_train_score = {}
log_test_score = {}

mlp_train_acc,mlp_test_acc,main_info['MLP'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'MLP')
mlp_train_score = {}
mlp_test_score = {}

knn_train_acc,knn_test_acc,main_info['KNN'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'KNN')
knn_train_score = {}
knn_test_score = {}

dt_train_acc,dt_test_acc,main_info['DT'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'DT')
dt_train_score = {}
dt_test_score = {}

nb_train_acc,nb_test_acc,main_info['NB'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'NB')
nb_train_score = {}
nb_test_score = {}

svc_train_acc,svc_test_acc,main_info['SVC']= score_best_acc(X.as_matrix(),Y.as_matrix(),'SVC')
svc_train_score = {}
svc_test_score = {}

ada_train_acc,ada_test_acc,main_info['ADA_BOOST'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'ADA')
ada_train_score = {}
ada_test_score = {}

rand_train_acc,rand_test_acc,main_info['RAND'] = score_best_acc(X.as_matrix(),Y.as_matrix(),'RAND')
rand_train_score = {}
rand_test_score = {}

for item in main_info:
    print item,main_info[item]

# for i,item1,item2 in zip(range(len(log_train_acc)),log_train_acc,log_test_acc):
#     log_train_score[i] = log_train_acc[item1]['avg_acc_score']
#     log_test_score[i] = log_test_acc[item2]['avg_acc_score']
#
#
# for i,item1,item2 in zip(range(len(mlp_train_acc)),mlp_train_acc,mlp_test_acc):
#     mlp_train_score[i] = mlp_train_acc[item1]['avg_acc_score']
#     mlp_test_score[i] = mlp_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(log_train_acc)),knn_train_acc,knn_test_acc):
#     knn_train_score[i] = knn_train_acc[item1]['avg_acc_score']
#     knn_test_score[i] = knn_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(mlp_train_acc)),dt_train_acc,dt_test_acc):
#     dt_train_score[i] = dt_train_acc[item1]['avg_acc_score']
#     dt_test_score[i] = dt_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(log_train_acc)),nb_train_acc,nb_test_acc):
#     nb_train_score[i] = nb_train_acc[item1]['avg_acc_score']
#     nb_test_score[i] = nb_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(mlp_train_acc)),svc_train_acc,svc_test_acc):
#     svc_train_score[i] = svc_train_acc[item1]['avg_acc_score']
#     svc_test_score[i] = svc_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(mlp_train_acc)),ada_train_acc,ada_test_acc):
#     ada_train_score[i] = ada_train_acc[item1]['avg_acc_score']
#     ada_test_score[i] = ada_test_acc[item2]['avg_acc_score']
#
# for i,item1,item2 in zip(range(len(mlp_train_acc)),rand_train_acc,rand_test_acc):
#     rand_train_score[i] = rand_train_acc[item1]['avg_acc_score']
#     rand_test_score[i] = rand_test_acc[item2]['avg_acc_score']
#
#
#
#
# plt.plot(range(2,13),log_train_score.values(),'o-',color="r",label="LRA")
# plt.plot(range(2,13),mlp_train_score.values(),'g^-',color="b",label="ANN")
# plt.plot(range(2,13),knn_train_score.values(),'bs-',color="g",label="KNN")
# plt.plot(range(2,13),dt_train_score.values(),'1-',color="c",label="DT")
# plt.plot(range(2,13),nb_train_score.values(),'>-',color="m",label="NB")
# plt.plot(range(2,13),svc_train_score.values(),'d-',color="y",label="SVM")
# plt.plot(range(2,13),ada_train_score.values(),'D-',color="m",label="AB")
# plt.plot(range(2,13),rand_train_score.values(),'x-',color="m",label="RF")
# plt.legend(loc="best")
# plt.xlabel('K-Folds')
# plt.ylabel('Accuracy for Train Part')
# plt.show()
#
#
# plt.plot(range(2,13),log_test_score.values(),'o-',color="r",label="LRA")
# plt.plot(range(2,13),mlp_test_score.values(),'g^-',color="b",label="ANN")
# plt.plot(range(2,13),knn_test_score.values(),'bs-',color="g",label="KNN")
# plt.plot(range(2,13),dt_test_score.values(),'1-',color="c",label="DT")
# plt.plot(range(2,13),nb_test_score.values(),'>-',color="m",label="NB")
# plt.plot(range(2,13),svc_test_score.values(),'d-',color="y",label="SVM")
# plt.plot(range(2,13),ada_test_score.values(),'D-',color="m",label="AB")
# plt.plot(range(2,13),rand_test_score.values(),'x-',color="m",label="RF")
# plt.legend(loc="best")
# plt.xlabel('K-Folds')
# plt.ylabel('Accuracy for Test Part')
# plt.show()
#
#
# main_accuracy_score = []
# name_of_main_accuracy_score = []
# name_of_main_accuracy_score.append('LRA')
# main_accuracy_score.append(log_test_score.values())
#
# name_of_main_accuracy_score.append('ANN')
# main_accuracy_score.append(mlp_test_score.values())
#
# name_of_main_accuracy_score.append('KNN')
# main_accuracy_score.append(knn_test_score.values())
#
# name_of_main_accuracy_score.append('DT')
# main_accuracy_score.append(log_test_score.values())
#
# name_of_main_accuracy_score.append('NB')
# main_accuracy_score.append(nb_test_score.values())
#
# name_of_main_accuracy_score.append('SVM')
# main_accuracy_score.append(svc_test_score.values())
#
# name_of_main_accuracy_score.append('AB')
# main_accuracy_score.append(ada_test_score.values())
#
# name_of_main_accuracy_score.append('RF')
# main_accuracy_score.append(rand_test_score.values())
#
# fig = plt.figure()
# fig.suptitle('Algorithm Comparison')
# ax = fig.add_subplot(111)
# plt.boxplot(main_accuracy_score)
# ax.set_xticklabels(name_of_main_accuracy_score)
# plt.show()




#("{'warm_start': False, 'kFold_split': 10, 'C': 0.001, 'solver': 'lbfgs', 'max_iter': 100, 'penalty': 'l1', 'multi_class': 'multinomial', 'random_state': 10, 'dual': False, 'class_weight': 'balanced'}", 0.98245614035087714)
#22 minutes for executing




def mlp(X_digit,Y_digit):
    main_start_time = time.time()
    mlp_activation = ['identity', 'logistic', 'tanh', 'relu']
    mlp_solver = ['lbfgs','sgd','adam']
    mlp_momentum = [0.3,0.6,0.9]#SGD
    mlp_learning_rate = ['constant', 'invscaling', 'adaptive']#SGD
    mlp_max_iter = [200,300,400]
    mlp_hidden_layer_sizes = [100,200]
    mlp_acc = {}
    start_time = time.time()
    for i in mlp_activation:
        for j in mlp_solver:
            for k in mlp_momentum:
                for l in mlp_learning_rate:
                    for c in mlp_max_iter:
                        for t in mlp_hidden_layer_sizes:
                            for kFoldValue in range(2,13):
                                inner_time = time.time()
                                temp = {}
                                temp['activation'] = i
                                temp['solver'] = j
                                temp['max_iter'] = c
                                temp['hidden_layer_sizes'] = t
                                if(j=='sgd'):
                                    mlp = MLPClassifier(activation=i,solver=j,momentum=k,learning_rate=l,max_iter=c,hidden_layer_sizes=t)
                                    temp['momentum'] = k
                                    temp['learning_rate'] = l
                                else:
                                    mlp = MLPClassifier(activation=i,solver=j,max_iter=c,hidden_layer_sizes=t)
                                acc_score_temp = []
                                kf = KFold(n_splits=kFoldValue)
                                for train_indices, test_indices in kf.split(X_digit):
                                    x_train, y_train, x_test, y_test = [], [], [], []
                                    for arr in X_digit[train_indices]:
                                        x_train.append(arr[0:-1])
                                    for arr in Y_digit[train_indices]:
                                        y_train.append(arr[0])
                                    for arr in X_digit[test_indices]:
                                        x_test.append(arr[0:-1])
                                    for arr in Y_digit[test_indices]:
                                        y_test.append(arr[0])
                                    x_train = np.array(x_train)
                                    y_train = np.array(y_train)
                                    x_test = np.array(x_test)
                                    y_test = np.array(y_test)

                                    mlp.fit(x_train, y_train)
                                    predicted = mlp.predict(x_test)
                                    acc = accuracy_score(y_test, predicted)
                                    acc_score_temp.append(acc)

                                temp['kFold_split'] = kFoldValue
                                if (max(acc_score_temp) != 1.0):
                                    mlp_acc[str(temp)] = max(acc_score_temp)
                                    print temp
                                    print max(acc_score_temp)
                                else:
                                    pass

	b = sorted(mlp_acc.iteritems(), key=lambda key_value: key_value[0])
	main_end_time = time.time() - main_start_time
	return b[0],main_end_time


# mlp_best,timeOfEx = mlp(X.as_matrix(),Y.as_matrix())
# print mlp_best
# print timeOfEx
#("{'hidden_layer_sizes': 100, 'learning_rate': 'adaptive', 'max_iter': 200, 'kFold_split': 10, 'solver': 'sgd', 'activation': 'identity', 'momentum': 0.3}", 0.98245614035087714)
#1320.11497211



def knn(X_digit,Y_digit):
    main_start_time = time.time()
    knn_n = [3,5,7,9,11]
    knn_w = ['uniform','distance']
    knn_p = [1,2,3,4]
    knn_leaf_size = [30,50,70]
    knn_acc = {}
    for i in knn_n:
        for j in knn_w:
            for k in knn_p:
                for l in knn_leaf_size:
                    for kFoldValue in range(2,12):
                        inner_time = time.time()
                        temp = {}
                        temp['n_neighbors'] = i
                        temp['weights'] = j
                        temp['leaf_size'] = l
                        temp['p'] = l
                        knn = KNeighborsClassifier(n_neighbors=i,weights=j,p = k,leaf_size = l)
                        acc_score_temp = []
                        kf = KFold(n_splits=kFoldValue)
                        for train_indices, test_indices in kf.split(X_digit):
                            x_train, y_train, x_test, y_test = [], [], [], []
                            for arr in X_digit[train_indices]:
                                x_train.append(arr[0:-1])
                            for arr in Y_digit[train_indices]:
                                y_train.append(arr[0])
                            for arr in X_digit[test_indices]:
                                x_test.append(arr[0:-1])
                            for arr in Y_digit[test_indices]:
                                y_test.append(arr[0])
                            x_train = np.array(x_train)
                            y_train = np.array(y_train)
                            x_test = np.array(x_test)
                            y_test = np.array(y_test)

                            knn.fit(x_train, y_train)
                            predicted = knn.predict(x_test)
                            acc = accuracy_score(y_test, predicted)
                            acc_score_temp.append(acc)

                        temp['kFold_split'] = kFoldValue
                        if (max(acc_score_temp) != 1.0):
                            knn_acc[str(temp)] = max(acc_score_temp)
                            print temp
                            print max(acc_score_temp)
                        else:
                            pass
	b = sorted(knn_acc.iteritems(), key=lambda key_value: key_value[0])
	main_end_time = time.time() - main_start_time
	return b[0],main_end_time

# knn_best,timeOfEx = knn(X.as_matrix(),Y.as_matrix())
# print knn_best
# print timeOfEx
# ("{'kFold_split': 10, 'n_neighbors': 3, 'weights': 'distance', 'leaf_size': 30, 'p': 30}", 0.96491228070175439)
# 9.09930896759


def dt(X_digit,Y_digit):
    main_start_time = time.time()
    dt_acc = {}
    dt_criterion = ['gini','entropy']
    dt_splitter = ['best','random']
    dt_min_samples_split = [2,4,6,8,10,20]
    dt_presort = [False,True]
    for i in dt_criterion:
        for j in dt_splitter:
            for k in dt_min_samples_split:
                for l in dt_presort:
                    for kFoldValue in range(2,12):
                        inner_time = time.time()
                        temp = {}
                        temp['criterion'] = i
                        temp['splitter'] = j
                        temp['min_samples_split'] = k
                        temp['presort'] = l
                        dt = DecisionTreeClassifier(criterion=i,splitter=j,min_samples_split=k,presort=l)
                        acc_score_temp = []
                        kf = KFold(n_splits=kFoldValue)
                        for train_indices, test_indices in kf.split(X_digit):
                            x_train, y_train, x_test, y_test = [], [], [], []
                            for arr in X_digit[train_indices]:
                                x_train.append(arr[0:-1])
                            for arr in Y_digit[train_indices]:
                                y_train.append(arr[0])
                            for arr in X_digit[test_indices]:
                                x_test.append(arr[0:-1])
                            for arr in Y_digit[test_indices]:
                                y_test.append(arr[0])
                            x_train = np.array(x_train)
                            y_train = np.array(y_train)
                            x_test = np.array(x_test)
                            y_test = np.array(y_test)

                            dt.fit(x_train, y_train)
                            predicted = dt.predict(x_test)
                            acc = accuracy_score(y_test, predicted)
                            acc_score_temp.append(acc)

                        temp['kFold_split'] = kFoldValue
                        if (max(acc_score_temp) != 1.0):
                            dt_acc[str(temp)] = max(acc_score_temp)
                            print temp
                            print max(acc_score_temp)
                        else:
                            pass
	b = sorted(dt_acc.iteritems(), key=lambda key_value: key_value[0])
	main_end_time = time.time() - main_start_time
	return b[0],main_end_time


# dt_best,timeOfEx = dt(X.as_matrix(),Y.as_matrix())
# print dt_best
# print timeOfEx
# ("{'min_samples_split': 10, 'splitter': 'best', 'criterion': 'gini', 'presort': False, 'kFold_split': 10}", 0.9642857142857143)
# 4.6391351223

def nb(X,Y):
	main_start_time = time.time()
	nb = GaussianNB()
	shuffle_type = [True,False]
	nb_acc = {}
	for i in range(2,12):
		for j in range(10):
			for k in shuffle_type:
				kf = KFold(n_splits=i,random_state=j)
				for train_index, test_index in kf.split(X):
					inner_time = time.time()
					temp = {}
					l = []
					X_train, X_test = X[train_index], X[test_index]
					Y_train, Y_test = Y[train_index], Y[test_index]
					nb.fit(X_train,Y_train)
					predicted = nb.predict(X_test)
					acc = accuracy_score(Y_test,predicted)
					temp['n_splits'] = i
					temp['random_state'] = j
					temp['shuffle_type'] = k
					a = str(train_index[0]) + '-' + str(train_index[-1])
					b = str(test_index[0]) + '-' + str(test_index[-1])
					l.append(a)
					l.append(b)
					temp['rangeOfKsplit'] = l
					inner_end_time = time.time() - inner_time
					temp['exec_time'] = inner_end_time
					nb_acc[acc] = temp
	main_end_time = time.time() - main_start_time
	b = sorted(nb_acc.iteritems(), key=lambda key_value: key_value[0])
	return b[0],main_end_time

# nb_best, timeOf = nb(X.as_matrix(),Y.as_matrix())
# print nb_best
# print timeOf
# (0.75, {'rangeOfKsplit': ['52-568', '0-51'], 'exec_time': 0.0009980201721191406, 'shuffle_type': False, 'random_state': 9, 'n_splits': 11})
# 1.75216293335

def svc(X_digit, Y_digit):
    svc_C = [0.01,0.1,1.0]
    svc_kernel = ['linear','poly','rbf']
    svc_degree = [1]
    svc_acc = {}
    for i in svc_C:
        for j in svc_kernel:
            for k in svc_degree:
                for kFoldValue in range(2,12):
                    temp = {}
                    temp['C'] = i
                    temp['kernel'] = j
                    temp['degree'] = k
                    svc = SVC(C=i,kernel=j,degree=k,probability=False,shrinking=False,class_weight='balanced',verbose=False)
                    acc_score_temp = []
                    kf = KFold(n_splits=kFoldValue)
                    for train_indices, test_indices in kf.split(X_digit):
                        x_train, y_train, x_test, y_test = [], [], [], []
                        for arr in X_digit[train_indices]:
                            x_train.append(arr[0:-1])
                        for arr in Y_digit[train_indices]:
                            y_train.append(arr[0])
                        for arr in X_digit[test_indices]:
                            x_test.append(arr[0:-1])
                        for arr in Y_digit[test_indices]:
                            y_test.append(arr[0])
                        x_train = np.array(x_train)
                        y_train = np.array(y_train)
                        x_test = np.array(x_test)
                        y_test = np.array(y_test)

                        svc.fit(x_train, y_train)
                        predicted = svc.predict(x_test)
                        acc = accuracy_score(y_test, predicted)
                        acc_score_temp.append(acc)

                    temp['kFold_split'] = kFoldValue
                    if (max(acc_score_temp) != 1.0):
                        svc_acc[str(temp)] = max(acc_score_temp)
                        print temp
                        print max(acc_score_temp)
                    else:
                        pass
    return svc_acc

# svc_best = svc(X.as_matrix(),Y.as_matrix())
# a = sorted(svc_best.iteritems(), key=lambda key_value: key_value[0])
# print a[0]
# ("{'kFold_split': 10, 'kernel': 'linear', 'C': 0.01, 'degree': 1}", 0.96491228070175439)

def adaboost(X_digit, Y_digit):
    adabost_base_est = ['LOG','DT','NB','SVC']
    adaboost_estimators = [50,100]
    adaboost_alg = ['SAMME','SAMME.R']
    adaboost_acc = {}
    for model in adabost_base_est:
        for num_est in adaboost_estimators:
            for alf in adaboost_alg:
                for kFoldValue in range(2,12):
                    temp = {}
                    temp['n_estimators'] = num_est
                    temp['alg'] = alf
                    if (model == 'LOG'):
                        MODEL = LogisticRegression(warm_start=False, C=0.001, solver='lbfgs', max_iter=100,
                                                   penalty='l2', multi_class='multinomial', random_state=10, dual=False,
                                                   class_weight='balanced')
                    elif (model == 'DT'):
                        MODEL = DecisionTreeClassifier(min_samples_split=10, splitter='best', criterion='gini',
                                                       presort=False)
                    elif (model == 'NB'):
                        MODEL = GaussianNB()
                    elif (model == 'SVC'):
                        MODEL = SVC(kernel='linear', C=0.01, degree=1, probability=True, verbose=False, shrinking=False,
                                    class_weight='balanced')
                    temp['MODEL'] = MODEL
                    base_model = AdaBoostClassifier(base_estimator=MODEL,n_estimators=num_est,algorithm=alf)
                    acc_score_temp = []
                    kf = KFold(n_splits=kFoldValue)
                    for train_indices, test_indices in kf.split(X_digit):
                        x_train, y_train, x_test, y_test = [], [], [], []
                        for arr in X_digit[train_indices]:
                            x_train.append(arr[0:-1])
                        for arr in Y_digit[train_indices]:
                            y_train.append(arr[0])
                        for arr in X_digit[test_indices]:
                            x_test.append(arr[0:-1])
                        for arr in Y_digit[test_indices]:
                            y_test.append(arr[0])
                        x_train = np.array(x_train)
                        y_train = np.array(y_train)
                        x_test = np.array(x_test)
                        y_test = np.array(y_test)

                        base_model.fit(x_train, y_train)
                        predicted = base_model.predict(x_test)
                        acc = accuracy_score(y_test, predicted)
                        acc_score_temp.append(acc)

                    temp['kFold_split'] = kFoldValue
                    if (max(acc_score_temp) != 1.0):
                        adaboost_acc[str(temp)] = max(acc_score_temp)
                        print temp
                        print max(acc_score_temp)
                    else:
                        pass
    return adaboost_acc


# ada_best = adaboost(X.as_matrix(),Y.as_matrix())
# a = sorted(ada_best.iteritems(), key=lambda key_value: key_value[0])
# print a[0]
# ("{'n_estimators': 100, 'alg': 'SAMME', 'MODEL': DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,\n            max_features=None, max_leaf_nodes=None,\n            min_impurity_split=1e-07, min_samples_leaf=1,\n            min_samples_split=10, min_weight_fraction_leaf=0.0,\n            presort=False, random_state=None, splitter='best'), 'kFold_split': 10}", 0.98245614035087714)

def random_forest(X_digit, Y_digit):
    rand_n_estimators = [5,10,15]
    rand_criterion = ['gini','entropy']
    rand_max_features = ['auto',None]
    rand_bootstrap = [True]
    rand_oob_score = [False]
    rand_verbose = [0,1,2]
    rand_warm_start = [True,False]
    class_weight = ['balanced']
    rand_acc = {}
    for n_estimator in rand_n_estimators:
        for criterion in rand_criterion:
            for features in rand_max_features:
                for bootstrap in rand_bootstrap:
                    for oob_score in rand_oob_score:
                        for verbose in rand_verbose:
                            for warm_smart in rand_warm_start:
                                for kFoldValue in range(2,12):
                                    temp = {}
                                    temp['n_estimators'] = n_estimator
                                    temp['criterion'] = criterion
                                    temp['features'] = features
                                    temp['bootstrap'] = bootstrap
                                    temp['oob_score'] = oob_score
                                    temp['verbose'] = verbose
                                    temp['warm_start'] = warm_smart
                                    base_model = RandomForestClassifier(n_estimators=n_estimator, criterion=criterion,max_features=features,bootstrap=bootstrap,oob_score=oob_score,verbose=verbose,warm_start=warm_smart,class_weight=class_weight[0])
                                    acc_score_temp = []
                                    kf = KFold(n_splits=kFoldValue)
                                    for train_indices, test_indices in kf.split(X_digit):
                                        x_train, y_train, x_test, y_test = [], [], [], []
                                        for arr in X_digit[train_indices]:
                                            x_train.append(arr[0:-1])
                                        for arr in Y_digit[train_indices]:
                                            y_train.append(arr[0])
                                        for arr in X_digit[test_indices]:
                                            x_test.append(arr[0:-1])
                                        for arr in Y_digit[test_indices]:
                                            y_test.append(arr[0])
                                        x_train = np.array(x_train)
                                        y_train = np.array(y_train)
                                        x_test = np.array(x_test)
                                        y_test = np.array(y_test)

                                        base_model.fit(x_train, y_train)
                                        predicted = base_model.predict(x_test)
                                        acc = accuracy_score(y_test, predicted)
                                        acc_score_temp.append(acc)

                                    temp['kFold_split'] = kFoldValue
                                    if (max(acc_score_temp) != 1.0):
                                        rand_acc[str(temp)] = max(acc_score_temp)
                                        print temp
                                        print max(acc_score_temp)
                                    else:
                                        pass
    return rand_acc

# rand_best = random_forest(X.as_matrix(),Y.as_matrix())
# a = sorted(rand_best.iteritems(), key=lambda key_value: key_value[0])
# print a[0]
# ("{'warm_start': False, 'kFold_split': 10, 'oob_score': False, 'features': 'auto', 'bootstrap': True, 'n_estimators': 10, 'criterion': 'entropy', 'verbose': 0}", 0.98245614035087714)
