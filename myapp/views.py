from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect

import os
import numpy as np
import pandas as pd

from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold

from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

file_name = open(os.path.join(BASE_DIR, 'myapp/static/files/dataset.txt'))
X = []
Y = []
class_names = ['Benign','Malignant']
class_names = np.array(class_names)

names=["ID", "Diagnosis", "F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","F16","F17","F18","F19","F20","F21","F22","F23","F24","F25","F26","F27","F28","F29","F30"]
name = ['Diagnosis'] #Target column name

df = pd.read_csv(file_name, sep=",", header=None, names=names)

X = pd.DataFrame(df).drop('Diagnosis',axis=1).drop('ID',axis=1).drop('F5',axis=1).drop('F6',axis=1).drop('F9',axis=1).drop('F10',axis=1).drop('F12',axis=1).drop('F15',axis=1).drop('F16',axis=1).drop('F17', axis=1).drop('F18',axis=1).drop('F19',axis=1).drop('F20',axis=1).drop('F22',axis=1).drop('F25',axis=1).drop('F26',axis=1).drop('F27',axis=1).drop('F28',axis=1).drop('F29',axis=1).drop('F30',axis=1)
Y = df['Diagnosis']


#logistic regression best params
#("{'warm_start': False, 'kFold_split': 10, 'C': 0.001, 'solver': 'lbfgs', 'max_iter': 100, 'penalty': 'l1', 'multi_class': 'multinomial', 'random_state': 10, 'dual': False, 'class_weight': 'balanced'}", 0.98245614035087714)


#mlp best params
#("{'hidden_layer_sizes': 100, 'learning_rate': 'adaptive', 'max_iter': 200, 'kFold_split': 10, 'solver': 'sgd', 'activation': 'identity', 'momentum': 0.3}", 0.98245614035087714)


#knn best params
#("{'kFold_split': 10, 'n_neighbors': 3, 'weights': 'distance', 'leaf_size': 30, 'p': 30}", 0.96491228070175439)


#decision tree best params
#("{'min_samples_split': 10, 'splitter': 'best', 'criterion': 'gini', 'presort': False, 'kFold_split': 10}", 0.9642857142857143)

#nb best params
#(0.75, {'rangeOfKsplit': ['52-568', '0-51'], 'exec_time': 0.0009980201721191406, 'shuffle_type': False, 'random_state': 9, 'n_splits': 11})


#svm best params
#("{'kFold_split': 10, 'kernel': 'linear', 'C': 0.01, 'degree': 1}", 0.96491228070175439)




def mlp_func(main_input,X_digit,Y_digit):
    mlp = MLPClassifier(hidden_layer_sizes=100, learning_rate='adaptive', max_iter=200, solver='sgd',
                        activation='identity', momentum=0.3)
    kf = KFold(n_splits=10)
    find = False
    while(find==False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)

            mlp.fit(x_train, y_train)
            predicted = mlp.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if(float(format(acc,'.12f')) >= 0.982456140351):
                mlp_res = mlp.predict(main_input)
                mlp_res_prob = mlp.predict_proba(main_input)
                find=True
                break

    return mlp_res,mlp_res_prob,acc

def log_func(main_input,X_digit,Y_digit):
    lr = LogisticRegression(warm_start= False, C= 0.001, solver = 'lbfgs', max_iter= 100, penalty = 'l2',multi_class='multinomial', random_state= 10, dual= False, class_weight='balanced')
    kf = KFold(n_splits=10)
    find = False
    while(find==False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)
            # print ('len' + str(len(x_train[0])))
            lr.fit(x_train, y_train)
            predicted = lr.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if(float(format(acc,'.12f')) >= 0.964912280702):
                mlp_res = lr.predict(main_input)
                mlp_res_prob = lr.predict_proba(main_input)
                find=True
                break

    return mlp_res,mlp_res_prob,acc

def knn_func(main_input,X_digit,Y_digit):
    knn = KNeighborsClassifier(n_neighbors=3, weights='distance', leaf_size=30 , p=30)
    kf = KFold(n_splits=10)
    find = False
    while (find == False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)
            knn.fit(x_train, y_train)
            predicted = knn.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if (float(format(acc,'.12f')) >= 0.964912280702):
                mlp_res = knn.predict(main_input)
                mlp_res_prob = knn.predict_proba(main_input)
                find = True
                break

    return mlp_res, mlp_res_prob,acc

def dt_func(main_input,X_digit,Y_digit):
    dt = DecisionTreeClassifier(min_samples_split=10, splitter='best', criterion= 'gini', presort=False)
    kf = KFold(n_splits=10)
    find = False
    while (find == False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)
            dt.fit(x_train, y_train)
            predicted = dt.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if (float(format(acc,'.12f')) >= 0.964912280702):
                mlp_res = dt.predict(main_input)
                mlp_res_prob = dt.predict_proba(main_input)
                find = True
                break

    return mlp_res, mlp_res_prob,acc

def nb_func(main_input,X_digit,Y_digit):
    nb = GaussianNB()
    kf = KFold(n_splits=11, random_state=9)
    find = False
    while (find == False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)
            nb.fit(x_train, y_train)
            predicted = nb.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if (float(format(acc,'.12f')) >= 0.9):
                mlp_res = nb.predict(main_input)
                mlp_res_prob = nb.predict_proba(main_input)
                find = True
                break

    return mlp_res, mlp_res_prob,acc

def svc_func(main_input,X_digit,Y_digit):
    svc = SVC(kernel='linear',C=0.01,degree=1,probability=True,verbose=False,shrinking=False,class_weight='balanced')
    kf = KFold(n_splits=10)
    find = False
    while (find == False):
        for train_indices, test_indices in kf.split(X_digit):
            x_train, y_train, x_test, y_test = [], [], [], []
            for arr in X_digit[train_indices]:
                x_train.append(arr[0:])
            for arr in Y_digit[train_indices]:
                y_train.append(arr[0])
            for arr in X_digit[test_indices]:
                x_test.append(arr[0:])
            for arr in Y_digit[test_indices]:
                y_test.append(arr[0])
            x_train = np.array(x_train)
            y_train = np.array(y_train)
            x_test = np.array(x_test)
            y_test = np.array(y_test)
            svc.fit(x_train, y_train)
            predicted = svc.predict(x_test)
            acc = accuracy_score(y_test, predicted)
            if (float(format(acc,'.12f')) >= 0.964912280702):
                mlp_res = svc.predict(main_input)
                mlp_res_prob = svc.predict_proba(main_input)
                find = True
                break

    return mlp_res, mlp_res_prob,acc

def index(request):
    newData = {}
    return render(request,'index.html',{'data':newData})

def second(request):
    if request.method == 'POST':
        name = request.POST['name']
        surname = request.POST['surname']
        age = request.POST['age']
        F1 = float(request.POST['f1'])
        F2 = float(request.POST['f2'])
        F3 = float(request.POST['f3'])
        F4 = float(request.POST['f4'])
        F7 = float(request.POST['f5'])
        F8 = float(request.POST['f6'])
        F11 = float(request.POST['f7'])
        F13 = float(request.POST['f8'])
        F14 = float(request.POST['f9'])
        F21 = float(request.POST['f10'])
        F23 = float(request.POST['f11'])
        F24 = float(request.POST['f12'])
        featuresData = [name,surname,age,F1,F2,F3,F4,F7,F8,F11,F13,F14,F21,F23,F24]
        s='{"data":['
        for i in featuresData:
            s+='"'+str(i)+'",'
        s=s[:len(s)-1]
        s+=']}'
        return render(request,'result.html',{'data':s})
    else:
        return HttpResponseRedirect('/')

def result(request):
    if request.method == 'GET':
        a = request.GET.get('input')
        a = a.split(',')
        info = a[:3]
        data = a[3:]
        print (len(data))
        print (len(X.as_matrix()[0]))
        for x in range(len(data)):
            data[x] = float(data[x])

        b = np.array(data).reshape(1,-1)

        mlp_res,mlp_res_prob,mlp_acc = mlp_func(b,X.as_matrix(),Y.as_matrix())
        mlp_output = str(mlp_res[0])+','+(str(max(mlp_res_prob[:,0],mlp_res_prob[:,1]))) + ',' + str(format(mlp_acc,'.4f'))+',.MLP_acc,.MLP_di,.MLP_prob,MLP'
        
        # main_output = str(mlp_acc)

        log_res,log_res_prob,log_acc= log_func(b,X.as_matrix(),Y.as_matrix())
        log_output = str(log_res[0])+','+ str(max(log_res_prob[:,0],log_res_prob[:,1])) + ',' + str(format(log_acc,'.4f'))+',.LOG_acc,.LOG_di,.LOG_prob,LOG'

        # main_output = str(log_acc)

        knn_res, knn_res_proba,knn_acc = knn_func(b, X.as_matrix(), Y.as_matrix())
        knn_output = str(knn_res[0]) + ',' + str(max(knn_res_proba[:,0],knn_res_proba[:,1])) + ',' + str(format(knn_acc,'.4f'))+',.KNN_acc,.KNN_di,.KNN_prob,KNN'


        # main_output = str(knn_acc)

        dt_res, dt_res_proba,dt_acc = dt_func(b, X.as_matrix(), Y.as_matrix())
        dt_output = str(dt_res[0]) + ',' + str(max(dt_res_proba[:,0],dt_res_proba[:,1])) + ',' + str(format(dt_acc,'.4f'))+',.DT_acc,.DT_di,.DT_prob,DT'


        # main_output = str(dt_acc)

        nb_res, nb_res_proba,nb_acc = nb_func(b, X.as_matrix(), Y.as_matrix())
        nb_output = str(nb_res[0]) + ',' + str(max(nb_res_proba[:,0],nb_res_proba[:,1])) + ',' + str(format(nb_acc,'.4f'))+',.NB_acc,.NB_di,.NB_prob,NB'


        # main_output = str(nb_acc)

        svc_res, svc_res_proba, svc_acc = svc_func(b, X.as_matrix(), Y.as_matrix())
        svc_output = str(svc_res[0]) + ',' + str(max(svc_res_proba[:,0],svc_res_proba[:,1])) +  ',' + str(format(svc_acc,'.4f'))+',.SVC_acc,.SVC_di,.SVC_prob,SVC'

        # main_output = str(svc_acc)


        main_output = mlp_output+';'+log_output+';'+knn_output+';'+dt_output+';'+nb_output +';'+ svc_output
        return HttpResponse(main_output)
    else:
        return HttpResponse('NOOO')

def stat(request):
    data = {}
    return render(request,'stat.html',{'data':data})

def get_stat(request):
    M_counter = 0
    B_counter = 0
    for i in range(len(Y)):
        if(Y[i]=='M'):
            M_counter+=1
        elif(Y[i]=='B'):
            B_counter+=1
        else:
            pass
    data = {
        'M_counter':M_counter,
        'B_counter':B_counter
    }

    str_counter = str(data['M_counter'])+','+str(data['B_counter'])
    return HttpResponse(str_counter)

def info(request):
    data = {}
    return render(request,'info.html',{'data':data})
